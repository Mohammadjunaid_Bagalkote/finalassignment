﻿using ShoppingCart.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShoppingCart.Controllers
{
    public class HomeController : Controller
    {
        ShoppingCartEntities db = new ShoppingCartEntities();
        public ActionResult Index()
        {
            if(TempData["cart"]!=null)
            {
                float x = 0;
                List<cart> li2 = TempData["cart"] as List<cart>;
                foreach(var item in li2)
                {
                    x = x + item.bill;
                }
                TempData["total"] = x;
            }
            TempData.Keep();
            return View(db.Products.OrderByDescending(x=>x.P_id).ToList());
        }
        public ActionResult AddToCart(int id)
        {
            
            
              //  var p = db.Products.Where(x => x.P_id == id).FirstOrDefault();
                return View(db.Products.Where(x => x.P_id == id).FirstOrDefault());
            
        }

        List<cart> li = new List<cart>();
        [HttpPost]
        public ActionResult AddToCart(Product pi,string qty,int Id)
        {
            Product p = db.Products.Where(x => x.P_id == Id).SingleOrDefault();

            cart c = new cart();
            c.productid = p.P_id;
            c.price = (float)p.Price;
            c.qty = Convert.ToInt32(p.P_MaxQty);
            c.bill = c.price * c.qty;
            c.productname = p.P_Name;
            if (TempData["cart"] == null)
            {
                li.Add(c);

                TempData["cart"] = li;
            }
            else
            {
                List<cart> li2 = TempData["cart"] as List<cart>;
                int flag = 0;
                foreach (var item in li2)
                {
                    if (item.productid == c.productid)
                    {
                        item.qty += c.qty;
                        item.bill += c.bill;
                        flag = 1;
                    }

                }
                if (flag == 0)
                {
                    li2.Add(c);
                }
                TempData["cart"] = li2;
            }
            TempData.Keep();
            return RedirectToAction("Index");

        }
        public ActionResult checkout()
        {
            TempData.Keep();
            return View();
        }
        [HttpPost]
        public ActionResult checkout(tb_Order o)
        {
            List<cart> li=TempData["cart"] as List<cart>;
           foreach(var item in li)
            {
                tb_Order od = new tb_Order();
                od.O_id = item.productid;
                od.O_Qty = item.qty;
                od.O_Price = (int)item.price;
                od.O_GrandTotal = item.bill;
                od.O_OrderDate = System.DateTime.Now;
                db.tb_Order.Add(od);
                db.SaveChanges();
                TempData["new"] = "database";
                

            }

            TempData.Remove("total");
            TempData.Remove("cart");
            TempData["msg"] = "Transaction completed.........";
            TempData.Keep();

            return RedirectToAction("Index");
        }
        public ActionResult remove(int? id)
        {
            List<cart> li2 = TempData["cart"] as List<cart>;
            cart c = li2.Where(x => x.productid == id).SingleOrDefault();
            li2.Remove(c);
            float h = 0;
            foreach(var item in li2)
            {
                h += item.bill;
            }
            TempData["total"] = h;

            return RedirectToAction("checkout");
        }
        public ActionResult Customer()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Customer(Customer c)
        {
            db.Customers.Add(c);
            db.SaveChanges();
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}