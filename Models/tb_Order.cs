//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShoppingCart.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_Order
    {
        public int O_id { get; set; }
        public Nullable<int> O_fk_Customer { get; set; }
        public Nullable<int> O_fk_Product { get; set; }
        public Nullable<double> O_Price { get; set; }
        public Nullable<double> O_Qty { get; set; }
        public Nullable<double> O_GrandTotal { get; set; }
        public Nullable<double> O_UnitPrice { get; set; }
        public Nullable<System.DateTime> O_OrderDate { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
    }
}
